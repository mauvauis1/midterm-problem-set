public abstract class Weapon {

    private String name;
    private int attackPoint;
    private int defensePoint;
    private int durabilityPoint;

    public Weapon(String name, int attackPoint, int defensePoint, int durabilityPoint) {
        this.name = name;
        this.attackPoint = attackPoint;
        this.defensePoint = defensePoint;
        this.durabilityPoint = durabilityPoint;
    }

    public int getAttackPoint() {
        return attackPoint;
    }

    public int getDefensePoint() {
        return defensePoint;
    }

    public int getDurabilityPoint() {
        return durabilityPoint;
    }

    public String getName() {
        return name;
    }

    public void strike(Character aCharacter){
        System.out.println(this.name+" will strike "+aCharacter.getName());
    }

    public void chantSpell(Character aCharacter){
        System.out.println("Chanting offensive spell from "+this.getName()+" against "+aCharacter.getName());
    }

    public void chantHealingPoem(Character aCharacter){
        System.out.println("Chanting healing spell to "+aCharacter.getName()+" using "+this.getName());
    }

    public void block(Character aCharacter){
        System.out.println(this.getName()+" block every attack commenced by "+aCharacter.getName());
    }

}
